const AUTH_HEADERS  = {
    X_OWNER_ADDRESS : "x-owner-address",
    X_PUBLIC_KEY : "x-public-key",
    X_EXPIRE_IN: 'x-expire-in',
    AUTHORIZATION: 'Authorization'
}

export {AUTH_HEADERS}