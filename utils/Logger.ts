import winston from "winston";
import winstonDaily from "winston-daily-rotate-file";
import { existsSync, mkdirSync } from 'fs';

export class Logger {
  private logDir: string;
  private logLevel: string;
  private readonly logger: winston.Logger;

  constructor(_logDir: string, _logLevel: string) {
    this.logDir = _logDir;
    this.logLevel = _logLevel;
    if (!existsSync(this.logDir)) {
        mkdirSync(this.logDir);
      }
    const logFormat = winston.format.printf(
      ({ timestamp, level, message }) => `${timestamp} ${level}: ${message}`
    );

    this.logger = winston.createLogger({
      format: winston.format.combine(
        winston.format.timestamp({
          format: "YYYY-MM-DD HH:mm:ss",
        }),
        logFormat
      ),
      transports: [
        new winstonDaily({
          level: this.logLevel,
          datePattern: "YYYY-MM-DD",
          dirname: this.logDir + "/app", // log file /logs/app/*.log in save
          filename: `%DATE%.log`,
          maxFiles: 30, // 30 Days saved
          json: false,
          zippedArchive: true,
        }),
        // error log setting
        new winstonDaily({
          level: "error",
          datePattern: "YYYY-MM-DD",
          dirname: this.logDir + "/error", // log file /logs/error/*.log in save
          filename: `%DATE%.log`,
          maxFiles: 30, // 30 Days saved
          // handleExceptions: true,
          json: false,
          zippedArchive: true,
        }),
      ],
    });

    this.logger.add(
      new winston.transports.Console({
        level: this.logLevel,
        format: winston.format.combine(
          winston.format.splat(),
          winston.format.colorize()
        ),
      })
    );

  
  }
  public stream = {
    write: (message: string) => {
      this.logger.info(message.substring(0, message.lastIndexOf("\n")));
    },
  };

  public getLogger = () => {
    return this.logger;
  }
}
