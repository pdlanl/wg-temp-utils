import {Response} from 'express'
import {Document} from 'mongoose'

export * from './auth.interface';
export * from './routes.interface'; 
export * from './map.interface'

export type IPayload<T = unknown>  = {
    data: T,
    signature?: string
}


export type IResponse<T = unknown>   = {
    data: T,
    message: string
}

export interface IObjectId {
    _id: string
}

export type IDBDocument<T> = T & Document
export type IWGResponse<T = unknown> = Response<IResponse<T>>


