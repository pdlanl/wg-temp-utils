import { IncomingHttpHeaders } from "http";
import { IObjectId, IPayload, IResponse } from ".";


interface IHeaderMetadata {
  headers: IncomingHttpHeaders;
  rawHeaders: string[];
  httpVersion: string;
  ip: string;
}
interface IAuthHeader {
  Authorization: string;
  'x-owner-address': string;
  'x-public-key': string;
  'x-expire-in': number;
}

interface IAuthData {
  isBlocked: boolean;
  isAuthorized: boolean;
  shadowAddress: string;
  expireIn: number;
}
interface ILoginRequestPayload extends IUserWallets  {
  signature: string;
  timestamp: number;
  lastLoginTimestamp?: number;
}

export type ILoginResponse = IResponse<IUser>; 

interface IAuthVerifyPayload {
    authorization:string;
    ownerAddress:string
}

export type IAuthVerifyResponse = IResponse<IAuthData>;



//User Interfaces
interface IUserWallets {
  ownerAddress: string;
  shadowAddress: string
}


interface IUser extends IUserWallets{
  isBlocked: boolean;
  lastLoginTimestamp: number;
  expiresIn?: number;
}



export interface LoginMeta {
  _id: string;
}

export interface ILoginHistory extends IUserWallets {
  loginTimestamp: number;
  logoutTimestamp?: number;
  metadata: LoginMeta;
}

export type {
  IAuthHeader,
  IHeaderMetadata,
  ILoginRequestPayload,
  IUser,
  IAuthVerifyPayload,
  IAuthData
};
