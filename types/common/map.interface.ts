import { ObjectId, Schema } from "mongoose";
import { IPayload, IResponse } from ".";

export interface IWorldData {
  hexId: string;
  nftId: string;
}

export interface IWorld extends IWorldData {
  isActive: boolean;
  isLocked: boolean;
  mazeId: ObjectId | string;
}

export interface IWorldDetail extends Omit<IWorld, "mazeId"> {
  maze: IMaze;
}

export interface IMaze {
  _id?: string;
  isActive: boolean;
  startTimestamp: number;
  governedBy: string;
  seedWord: string;
}

export enum EnumItemInventorySource {
  PURCHASE = "purchase",
  EXPLORE = "explore",
  OTHERS = "others",
}

export interface IChracterItemInventory {
  _id?: string;
  quantity: number;
  sourceId: string;
  source: EnumItemInventorySource;
}

export interface ICharacterHealthAttribute {
  value: number;
  isCamping: boolean;
  regenerateTimestamp: number;
}

export interface IChracterGameAttribute {
  health: ICharacterHealthAttribute;
}

export interface IMazeCharacter {
  _id?: string;
  nft: string;
  maze: IMaze;
  areaId: string;
  itemInventory: Array<IChracterItemInventory>;
  gameAttributes: IChracterGameAttribute;
}

export interface IMoveCharacterPayload {
  mazeId: string;
  characterId: string;
  newPosition: string;
}

export type IWorldCampResponse = IResponse<IWorldData>;
export type IWorldCampRequestPayload = IPayload<IWorldData>;
