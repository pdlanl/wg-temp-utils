"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Logger = void 0;
const tslib_1 = require("tslib");
const winston_1 = tslib_1.__importDefault(require("winston"));
const winston_daily_rotate_file_1 = tslib_1.__importDefault(require("winston-daily-rotate-file"));
const fs_1 = require("fs");
class Logger {
    constructor(_logDir, _logLevel) {
        this.stream = {
            write: (message) => {
                this.logger.info(message.substring(0, message.lastIndexOf("\n")));
            },
        };
        this.getLogger = () => {
            return this.logger;
        };
        this.logDir = _logDir;
        this.logLevel = _logLevel;
        if (!(0, fs_1.existsSync)(this.logDir)) {
            (0, fs_1.mkdirSync)(this.logDir);
        }
        const logFormat = winston_1.default.format.printf(({ timestamp, level, message }) => `${timestamp} ${level}: ${message}`);
        this.logger = winston_1.default.createLogger({
            format: winston_1.default.format.combine(winston_1.default.format.timestamp({
                format: "YYYY-MM-DD HH:mm:ss",
            }), logFormat),
            transports: [
                new winston_daily_rotate_file_1.default({
                    level: this.logLevel,
                    datePattern: "YYYY-MM-DD",
                    dirname: this.logDir + "/app",
                    filename: `%DATE%.log`,
                    maxFiles: 30,
                    json: false,
                    zippedArchive: true,
                }),
                // error log setting
                new winston_daily_rotate_file_1.default({
                    level: "error",
                    datePattern: "YYYY-MM-DD",
                    dirname: this.logDir + "/error",
                    filename: `%DATE%.log`,
                    maxFiles: 30,
                    // handleExceptions: true,
                    json: false,
                    zippedArchive: true,
                }),
            ],
        });
        this.logger.add(new winston_1.default.transports.Console({
            level: this.logLevel,
            format: winston_1.default.format.combine(winston_1.default.format.splat(), winston_1.default.format.colorize()),
        }));
    }
}
exports.Logger = Logger;
//# sourceMappingURL=Logger.js.map