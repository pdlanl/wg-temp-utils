import winston from "winston";
export declare class Logger {
    private logDir;
    private logLevel;
    private readonly logger;
    constructor(_logDir: string, _logLevel: string);
    stream: {
        write: (message: string) => void;
    };
    getLogger: () => winston.Logger;
}
