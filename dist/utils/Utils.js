"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Utils = void 0;
class Utils {
}
exports.Utils = Utils;
Utils.sortJSON = (jsonObj) => {
    // check if json object is not empty and is of object type
    if (jsonObj && typeof jsonObj === 'object') {
        // get the keys of the object
        const keys = Object.keys(jsonObj);
        // sort the keys
        keys.sort();
        // create a new empty object
        const sortedObj = {};
        // add the key-value pairs to the new object in sorted order
        keys.forEach(key => {
            sortedObj[key] = jsonObj[key];
        });
        // return the sorted object
        return sortedObj;
    }
    // if json object is empty or is not of object type return the same object
    return jsonObj;
};
//# sourceMappingURL=Utils.js.map