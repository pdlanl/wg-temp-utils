"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.AUTH_HEADERS = void 0;
const AUTH_HEADERS = {
    X_OWNER_ADDRESS: "x-owner-address",
    X_PUBLIC_KEY: "x-public-key",
    X_EXPIRE_IN: 'x-expire-in',
    AUTHORIZATION: 'Authorization'
};
exports.AUTH_HEADERS = AUTH_HEADERS;
//# sourceMappingURL=index.js.map