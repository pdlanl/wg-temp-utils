declare const AUTH_HEADERS: {
    X_OWNER_ADDRESS: string;
    X_PUBLIC_KEY: string;
    X_EXPIRE_IN: string;
    AUTHORIZATION: string;
};
export { AUTH_HEADERS };
