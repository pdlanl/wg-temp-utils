"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpClient = void 0;
const tslib_1 = require("tslib");
const axios_1 = tslib_1.__importDefault(require("axios"));
const HttpException_1 = require("../exceptions/HttpException");
class Axios {
    constructor(config) {
        return axios_1.default.create(config);
    }
}
class HttpClient extends Axios {
    constructor(baseURL, timeout = 30000) {
        super({
            baseURL,
            timeout: timeout,
        });
        this.success = (response) => {
            return response.data;
        };
        this.error = (error) => {
            var _a, _b, _c, _d, _e;
            const { status, statusText } = error.response;
            const message = ((_b = (_a = error.response) === null || _a === void 0 ? void 0 : _a.data) === null || _b === void 0 ? void 0 : _b.message) || statusText;
            const stack = (_e = (_d = (_c = error.response) === null || _c === void 0 ? void 0 : _c.data) === null || _d === void 0 ? void 0 : _d.stack) !== null && _e !== void 0 ? _e : error.stack;
            throw new HttpException_1.HttpException(status, message, stack);
        };
        this.request = this.request.bind(this);
        this.get = this.get.bind(this);
        this.options = this.options.bind(this);
        this.post = this.post.bind(this);
    }
    request(config) {
        return this.request(config);
    }
    get(url, config) {
        return this.get(url, config);
    }
    options(url, config) {
        return this.options(url, config);
    }
    post(url, data, config) {
        return this.post(url, data, config);
    }
}
exports.HttpClient = HttpClient;
//# sourceMappingURL=http.client.service.js.map