import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';
declare class Axios {
    constructor(config: AxiosRequestConfig);
}
export declare abstract class HttpClient extends Axios {
    protected constructor(baseURL: string, timeout?: number);
    request<T, R = AxiosResponse<T>>(config: AxiosRequestConfig): Promise<R>;
    get<T, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R>;
    options<T, R = AxiosResponse<T>>(url: string, config?: AxiosRequestConfig): Promise<R>;
    post<T, B, R = AxiosResponse<T>>(url: string, data?: B, config?: AxiosRequestConfig): Promise<R>;
    success: <T>(response: AxiosResponse<T, any>) => T;
    error: (error: AxiosError<Error>) => never;
}
export {};
