"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.HttpException = void 0;
const tslib_1 = require("tslib");
const http_status_1 = tslib_1.__importDefault(require("http-status"));
class HttpException extends Error {
    constructor(status, message, stack = '', errors = []) {
        super(message);
        this.name = this.constructor.name;
        this.status = status || http_status_1.default.INTERNAL_SERVER_ERROR;
        this.message = message;
        this.errors = errors;
        this.status = status;
        this.stack = stack;
    }
}
exports.HttpException = HttpException;
//# sourceMappingURL=HttpException.js.map