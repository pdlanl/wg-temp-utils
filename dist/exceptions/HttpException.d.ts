export declare class HttpException extends Error {
    status: number;
    message: string;
    errors: string[];
    constructor(status: number, message: string, stack?: string, errors?: any[]);
}
