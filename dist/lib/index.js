"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
const tslib_1 = require("tslib");
const WGLib = tslib_1.__importStar(require("./wg"));
tslib_1.__exportStar(require("../types"), exports);
tslib_1.__exportStar(require("../constants"), exports);
exports.default = WGLib;
//# sourceMappingURL=index.js.map