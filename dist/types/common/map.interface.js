"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.EnumItemInventorySource = void 0;
var EnumItemInventorySource;
(function (EnumItemInventorySource) {
    EnumItemInventorySource["PURCHASE"] = "purchase";
    EnumItemInventorySource["EXPLORE"] = "explore";
    EnumItemInventorySource["OTHERS"] = "others";
})(EnumItemInventorySource = exports.EnumItemInventorySource || (exports.EnumItemInventorySource = {}));
//# sourceMappingURL=map.interface.js.map