export interface IMessageQueue {
    init: () => Promise<this>;
    createExchange: (name: string) => Promise<this>;
    publish: (exchange: string, route: string, msg: string) => Promise<this>;
    subscribe: (exchange: string, route: string, handler: (msg: string, ack: any) => Promise<void>) => Promise<() => void>;
}
